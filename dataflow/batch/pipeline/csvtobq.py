"""
A module focused on pipeline building
"""
import apache_beam as beam
from apache_beam.io import ReadFromText, WriteToBigQuery
from apache_beam import pvalue
from utils.utils import parse_schema_dict, extract_schema
from apache_beam.io.filebasedsource import *

from dataflow.batch.dofn.parsedata import ParseDataDoFn

import csv


class MyCsvFileSource(FileBasedSource):
    def read_records(self, file_name, range_tracker):
        self._file = self.open_file(file_name)
        reader = csv.reader(self._file, delimiter='|', quotechar='"')

        for rec in reader:
            line = '|'.join(rec).encode('utf-8')
            yield line


def build_pipeline(pipeline_options):
    """
    Build a dataflow batch pipeline that reads from a txt file in GS, split it
    and writing the result in mdm table
    in BigQuery
    :type pipeline_options:
    :return: the pipeline to run
    """

    pipeline = beam.Pipeline(options=pipeline_options)

    bq_schema = parse_schema_dict(extract_schema(pipeline_options.table_name))

    # Recuperation de toutes les lignes
    # lines = (pipeline | 'Read Text file from RL1' >> ReadFromText(pipeline_options.input, min_bundle_size=0,
    #                                                               skip_header_lines=pipeline_options.skip_header_lines))

    (pipeline
     | beam.io.Read(MyCsvFileSource(pipeline_options.input))
     | beam.Map(lambda line: print_each_line(line)))

    # # gestion d un fichier avec une ligne d'entete avec un numero de version
    # if pipeline_options.skip_header_lines == 0 and pipeline_options.id_header_line != '+_':
    #
    #     version = (lines | 'Extract Version line' >> beam.Filter(
    #         lambda x: x.startswith(pipeline_options.id_header_line))
    #                | 'Extract Version Number' >>
    #                beam.Map(lambda x: x.split(pipeline_options.separator)[pipeline_options.column_version]))
    #
    #     results = (lines | 'Filter first line' >> beam.Filter(
    #         lambda x: not x.startswith(pipeline_options.id_header_line))
    #                | 'Build BQ Row' >> beam.ParDo(ParseDataDoFn(), bq_schema, pipeline_options.input,
    #                                               pipeline_options.separator, pvalue.AsSingleton(version)))
    # else:
    #     # gestion d un fichier sans ligne d entete ou avec une ligne d entete sans version
    #     results = (lines | 'Build BQ Row' >> beam.ParDo(ParseDataDoFn(), bq_schema, pipeline_options.input,
    #                                                     pipeline_options.separator))
    #
    # results | 'Write in BQ Table' >> WriteToBigQuery(
    #     pipeline_options.table_name,
    #     create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
    #     write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND)

    return pipeline


def print_each_line(line):
    print 'Ligne => ' + line
