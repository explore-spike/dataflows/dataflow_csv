"""
a beam.DoFn implementation to parse field from a CSV line before yielding a Dict
"""
import csv
from datetime import datetime
import logging
from collections import OrderedDict

import apache_beam as beam
from utils import utils


class ParseException(Exception):
    """
    A custom exception class to be raisen for parsing exception
    """
    pass


class ParseDataDoFn(beam.DoFn):
    """
    The DoFn implementation
    """

    def __init__(self):
        super(ParseDataDoFn, self).__init__()

    def process(self, element, schema, file_path, separator, version=None):
        """
        Parse CSV line with separator '|' or ',' or ';' and build dict object with appropriate schema
        :param element: A line of the parsed file : str
        :param schema: schema of the destination table Metier Produit : dict
        :param file_path: File path of the file to transform : str
        :return: a Dict representation of data
        """
        try:
            # Encode and convert string line in list
            # The line string must be encode to 'utf-8' to prevent exception when the line contains
            # special character
            splitted_columns = list(csv.reader([element.encode("utf-8").strip()], delimiter=separator))[0]
        except ParseException:
            logging.error('Parse error on "%s"', element)
            raise ParseException

        # Match the input line with the appropiate schema and return a dict

        line_data = OrderedDict(zip(schema.keys(), splitted_columns))

        # Add metadata : Source_filename ,Processing time and version_number
        if version:
            line_data['version_number'] = version
        line_data['source_file_name'] = utils.extract_file_name(file_path)
        line_data['processing_time'] = str(datetime.now())

        yield line_data
