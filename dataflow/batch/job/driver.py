"""
Dataflow batch job driver
"""
from __future__ import absolute_import

from dataflow.batch.job.options import build_pipeline_options
from dataflow.batch.pipeline.csvtobq import build_pipeline


def run(argv=None):
    """
    The main function orchestrating building the pipeline options, the pipeline,
    running it and waiting util it finishes
    :param argv: job arguments
    :return:
    """

    pipeline_options = build_pipeline_options(argv)

    pipeline = build_pipeline(pipeline_options)

    result = pipeline.run()

    result.wait_until_finish()
