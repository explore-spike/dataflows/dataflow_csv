
"""
Module focused on arguments parsing and pipeline options building
"""

from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions


# Classe personnalise pour les parametres d'entrees de Dataflow pour les afficher dans
# la console Google
class MyOptions(PipelineOptions):

    @classmethod
    def _add_argparse_args(cls, parser):
        # Nom complet de la table de depart
        parser.add_argument('--input',
                            dest='input',
                            required=True,
                            help='Input file to process.')

        parser.add_argument('--table_name',
                            dest='table_name',
                            required=True,
                            help='Name of table')

        parser.add_argument('--separator',
                            dest='separator',
                            required=True,
                            help='Separator of the file')
        
        parser.add_argument('--skip_header_lines',
                            dest='skip_header_lines',
                            type=int,
                            default=0,
                            help='Number of row to skip')

        parser.add_argument('--id_header_line',
                            dest='id_header_line',
                            default='+_',
                            help='First character of header line')

        parser.add_argument('--column_version',
                            dest='column_version',
                            type=int,
                            default=-1,
                            help='Column of version number, begins at 0')


def build_pipeline_options(argv):
    """
    Parse arguments
    :param argv: job arguments
    :return: parsed arguments end pipeline options
    """

    pipeline_options = MyOptions(argv)
    pipeline_options.view_as(SetupOptions).save_main_session = True

    return pipeline_options
