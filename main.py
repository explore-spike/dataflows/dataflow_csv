"""
The dataflow job entry point 
"""
import logging
from dataflow.batch.job import driver

if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    driver.run()
