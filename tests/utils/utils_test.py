from unittest import TestCase
from utils import utils


class UtilsTest(TestCase):

    def test_extract_file_name(self):

        file_path_1 = 'gs://bck-mydata-collectstore-yrfr-dev-rl-sales-retail-iliade/2019-02-22' \
                      '/VTE_SOC_GCP_YRFR_D_DET_20190222030147.txt '
        file_path_2 = 'gs://bck-mydata-collectstore-yrfr-dev-rl-sales-retail-iliade/2019-02-22' \
                      '/VTE_SOC_GCP_YRFR_D_DET_20190222030147.txt.serial_number'

        file_name_1 = utils.extract_file_name(file_path_1)
        file_name_2 = utils.extract_file_name(file_path_2)

        self.assertEqual(file_name_1, 'VTE_SOC_GCP_YRFR_D_DET_20190222030147.txt')
        self.assertEqual(file_name_2, 'VTE_SOC_GCP_YRFR_D_DET_20190222030147.txt.serial_number')

    def test_convert_to_date(self):
        date_string = '20191202'

        formated_date = utils.convert_to_date(date_string)

        self.assertEqual(formated_date, '2019-12-02')

    def test_parse_bqstr_schema(self):
        schema_string = 'col1:INTEGER,col2:STRING,col3:DATE'

        schema_dict = utils.parse_bqstr_schema(schema_string)

        self.assertEqual(schema_dict, dict(col1='INTEGER', col2='STRING', col3='DATE'))

    def test_encode_to_str_utf8(self):
        var_to_encode = u'chaine_de_caractere'

        encoded_char = utils.encode_to_str_utf8(var_to_encode)

        self.assertEqual(encoded_char, 'chaine_de_caractere')
