# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class ParsedataDoFnTest(TestCase):
    """
    sample data avec numero de version
    YR|FR|4757|1|0074|2019-05-20 14:45:29|P01|2017-10-24 00:00:00|1900-01-01 7::00|1900-01-01 00:00:00|0|0|1

    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "CPT_LYR_DAT_YRFR_V_CPT.TXT"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/traffic_retail.json')
        self.version = 5

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_prospection_vad(self):
        line_prospection = u'YR|FR|4757|1|0074|2019-05-20 14:45:29|P01|2017-10-24 00:00:00|1900-01-01 7::00|1900-01-01 00:00:00|0|0|1'

        parsed_line = self.dofn.process(line_prospection, self.schema, self.file_path,
                                        self.separator, self.version).next()

        expected_values = ["YR","FR","4757","1","0074","2019-05-20 14:45:29","P01","2017-10-24 00:00:00","1900-01-01 7::00","1900-01-01 00:00:00","0","0","1"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time', 'version_number']:
                self.assertEqual(parsed_line[column], expected_values[idx])
            elif column == 'version_number':
                self.assertEqual(parsed_line[column], self.version)
        print expected_values
