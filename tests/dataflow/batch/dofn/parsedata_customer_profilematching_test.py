# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class parsedataDoFnTest(TestCase):
    """
    sample data
    04C87B82B52F8D83E053CD12A8C0BA79|
    949628892|VPM|7fa18124bc2414525f04edbe7eef26c880eeab51cce7eae9c26b42accb406771|
    df340084d26e78450d143675c49c586485e87ce351ebb42664a4c58a89359ca8|
    f0b408497864a4885aa704b3b4f60f3c8f802742c75da451690490a6a7dfeb95|
    d7f9756f640fd6a66204b5290c5d91a286ac40087707cc2d85aa4c8f192e29a3
    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "20190505_17830_INFOS_CLIENTES_PROFILMATCHEES_extrait"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/customer_profilematching.json')

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_customer_profilematching(self):
        line_article = u"04C87B82B52F8D83E053CD12A8C0BA79|949628892|VPM|" \
                        "7fa18124bc2414525f04edbe7eef26c880eeab51cce7eae9c26b42accb406771|" \
                        "df340084d26e78450d143675c49c586485e87ce351ebb42664a4c58a89359ca8|" \
                        "f0b408497864a4885aa704b3b4f60f3c8f802742c75da451690490a6a7dfeb95|" \
                        "d7f9756f640fd6a66204b5290c5d91a286ac40087707cc2d85aa4c8f192e29a3"

        parsed_line = self.dofn.process(line_article, self.schema, self.file_path, self.separator).next()

        expected_values = ["04C87B82B52F8D83E053CD12A8C0BA79", "949628892", "VPM",
                           "7fa18124bc2414525f04edbe7eef26c880eeab51cce7eae9c26b42accb406771",
                           "df340084d26e78450d143675c49c586485e87ce351ebb42664a4c58a89359ca8",
                           "f0b408497864a4885aa704b3b4f60f3c8f802742c75da451690490a6a7dfeb95",
                           "d7f9756f640fd6a66204b5290c5d91a286ac40087707cc2d85aa4c8f192e29a3"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time']:
                self.assertEqual(parsed_line[column], expected_values[idx])
