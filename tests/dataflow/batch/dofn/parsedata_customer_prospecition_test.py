# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class ParsedataDoFnTest(TestCase):
    """
    sample data avec numero de version
    1|"YR"|28|"FR"|1|"VAD"|"20110101"|"20110101"|34883681|"368434787"|215497|"0Q3K"|105|"20110215"|4490|"20110309"
    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "PRS_DIA_GCP_YRFR_D_DET.txt"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/prospection_vad.json')
        self.version = 5

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_prospection_vad(self):
        line_prospection = u'1|"YR"|28|"FR"|1|"VAD"|"20110101"|"20110101"|34883681|"368434787"|' \
                           '215497|"0Q3K"|105|"20110215"|4490|"20110309"'

        parsed_line = self.dofn.process(line_prospection, self.schema, self.file_path,
                                        self.separator, self.version).next()

        expected_values = ["1", "YR", "28", "FR", "1", "VAD", "20110101", "20110101", "34883681", "368434787",
                           "215497", "0Q3K", "105", "20110215", "4490", "20110309"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time', 'version_number']:
                self.assertEqual(parsed_line[column], expected_values[idx])
            elif column == 'version_number':
                self.assertEqual(parsed_line[column], self.version)
        print expected_values
