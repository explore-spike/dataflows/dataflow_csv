# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class ParsedataDoFnTest(TestCase):
    """
    sample data
    04C87B82B52F8D83E053CD12A8C0BA79|
    949628892|VPM|7fa18124bc2414525f04edbe7eef26c880eeab51cce7eae9c26b42accb406771|
    df340084d26e78450d143675c49c586485e87ce351ebb42664a4c58a89359ca8|
    f0b408497864a4885aa704b3b4f60f3c8f802742c75da451690490a6a7dfeb95|
    d7f9756f640fd6a66204b5290c5d91a286ac40087707cc2d85aa4c8f192e29a3
    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "VTE_SOC_GCP_YRFR_D_HIS.txt"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/history_socle.json')

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_history_socle(self):
        line_article = u'541131843|1|20|"20190413 11:46:00"|"153 "|154|"0435"|35614|"5501"||8148|' \
                        '"20190413"|"635594918"||"90"|||||0.00|0.00|0.00|1|"EUR"|"20190414 00:00:00"|' \
                        '"20190414 00:00:00"|1|68|2|"N"|0|"UNKNOW"|0.00|0|-1|||5034736981|"0000000000"|' \
                        '0|0.00|1|"N"|"N"|0.00|0.00|0.00|0|0|0|20|0|0|0|"1"|"N"|"Autre"||||||||||||' \
                        '2|31|"90"|130696|"72543"|35614|"5501"|0|"UNKNOW"||1||0|49.90|49.90|1|0.00|' \
                        '0.00|0.00|"EUR"|20.000|0.00|0.00|1.08|"0000000000"|0|44|"28"|24686|"053822"|' \
                        '0|-1||||0.00|0.00|0.00|0.00|0.00|0.00|0.00|41.58|"0"|0|||0|||0|' \
                        '|||||||||||||||||||||||||' \

        parsed_line = self.dofn.process(line_article, self.schema, self.file_path, self.separator).next()

        expected_values = ["541131843", "1", "20", "20190413 11:46:00", "153 ", "154", "0435", "35614", "5501", "",
                           "8148", "20190413", "635594918", "", "90", "", "", "", "", "0.00", "0.00", "0.00", "1",
                           "EUR", "20190414 00:00:00", "20190414 00:00:00", "1", "68", "2", "N", "0", "UNKNOW", "0.00",
                           "0", "-1", "", "", "5034736981", "0000000000", "0", "0.00", "1", "N", "N", "0.00", "0.00", "0.00",
                           "0", "0", "0", "20", "0", "0", "0", "1", "N", "Autre", "", "", "", "", "", "", "", "", "",
                           "", "", "2", "31", "90", "130696", "72543", "35614", "5501", "0", "UNKNOW", "", "1", "", "0",
                           "49.90", "49.90", "1", "0.00", "0.00", "0.00", "EUR", "20.000", "0.00", "0.00", "1.08",
                           "0000000000", "0", "44", "28", "24686", "053822", "0", "-1", "", "", "", "0.00", "0.00", "0.00",
                           "0.00", "0.00", "0.00", "0.00", "41.58", "0", "0", "", "", "0", "", "", "0", "", "", "", "",
                           "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time']:
                self.assertEqual(parsed_line[column], expected_values[idx])
        print expected_values
