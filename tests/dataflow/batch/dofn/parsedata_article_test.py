# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class parsedataDoFnTest(TestCase):
    """
    sample data
    71849483;312;5150532819;MAQUILLAGE;LUMINELLE 4;115.5;...
    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = ';'
        self.file_path = "VTE_LYR_SOC_YRFR_D_DET_20190106011128.txt"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/product.json')

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_mdm(self):
        line_article = u"1210v0;1210;0;Archivé;;3660005183077;0;Y00381A001A;18307;18307;;;;;" \
                        "15/01/2013;01/06/2019;01" \
                       u"/01/2018;;;SVC NUTRI 2 MASQ NUTRI P150ML;NUTRI SILKY MASK DRY HAIR 150ML" \
                        ";Nutrition - Masque " \
                       u"Nutri Soyeux Pot 150Ml;Nutrition - Nutri Silky Mask 150 Ml " \
                       u"Jar;;0;1.24;;36;N1302;S1608;7;;2;1;;Une efficacité boostée grâce" \
                       u" à l'avocat relipidant et " \
                       u"l'action des fructanes d'ageve dès le cuir chevelu, une texture" \
                        " sensorielle, un nouveau " \
                       u"parfum addictif et ce dans un tube ultra pratique.;" \
                       u"Une efficacité boostée grâce à l'avocat " \
                       u"relipidant et l'action des fructanes d'ageve dès le cuir chevelu," \
                        " une texture sensorielle, " \
                       u"un nouveau parfum addictif et ce dans un tube ultra pratique.;" \
                        "Produit inclus dans le " \
                       u"relancement juin 2019;Produit inclus dans le relancement juin " \
                       u"2019;;Oui;MARK;15/01/2013;01/08/2016;Non;;EUR;Archivé;Archived;" \
                        "1;Y00381A001;1;Oui;;;;122169" \
                       u";;;;https://assetsp.keepeek.com/pm_74_122_122169-067b412189-preview.jpg;" \
                        "https://assetsp" \
                       u".keepeek.com/pm_74_122_122169-067b412189-preview.jpg;SOIN VEGETAL CAP" \
                        " 2 MASQUE CHEVEUX " \
                       u"NUTRI-SOYEUX  POT 150ML;SOIN VEGETAL CAP 2 NUTRI-SILKY HAIR MASK  POT " \
                       u"150ML;;;;;1;;;CA|CZ|FR|GB|HU|MA|PL|RO|RU|SK|UA|US;01|03;;;5292v0;;0011;" \
                        "195.05;150;G;;;;18/04" \
                       u"/2018;22/03/2019;;553v0;553;0;Archivé;Non;Y00381A;;;;278;3305900000;1;;" \
                        "Oui;;;71;15/01/2013" \
                       u";01/08/2016;15/01/2013;01/06/2019;SVC Masque Chev;SVC Nutri-Silky;" \
                        "SVC-Masque Cheveux " \
                       u"Nutri-Soyeux-150ML;SVC-Masque Cheveux Nutri-Soyeux-150ML;" \
                        "Soin Vegetal Cap 2 Nutrition Masque " \
                       u"Cheveux Nutri-Soyeux  150ML;Soin Vegetal Cap 2 " \
                        "Nutrition Nutri-Silky Hair Mask  " \
                       u"150ML;Nutrition - Masque Soin Nutri-Soyeux;Nutrition - Masque Soin " \
                       u"Nutri-Soyeux;;;2;Non;MARK;;1;36;93;93;;;42;;42;;0;93;93;;;Non;;;Non;;;;" \
                        "9;IE;IE;0006;Non;;;1" \
                       u";;;;;;;;;;;;;;;;;;MM;;;;;;;;;Masque Cheveux Nutri 150ML;Nutri-Silky " \
                        "Hair Mas " \
                       u"150ML;;;GO;;;;;;;;;;;;1;;1;;;;;;;;;;;;553v1;;;;18/04/2018;06/10/2018;" \
                        ";0006;381v0;381;0" \
                       u";Archivé;;;41;;;Y00381;;15/01/2013;01/06/2019;Masque Cheveux " \
                        "Nutri-Soyeux;Nutri-Silky Hair " \
                       u"Mask;Masque Chev;Nutri-Silky;Masque Cheveux Nutri;Nutri-Silky Hair " \
                       u"Mas;;;;Non;;06;;;01;;;Non;0;;;;;;;;;;00004;00033;00000330;521;;;;87;" \
                        ";;101;;;;;;;;1;15/01" \
                       u"/2013;01/08/2016;Oui;;;;;;;;;;;17;;4;;;06;;;;165;18/04/2018;27/09/2018;" \
                        "278;15/01/2013;01" \
                       u";;;;;;;;SVC 15/01/2013 Shampooing Texturisant;launch of SVC" \
                        " BRILLANCE SH " \
                       u"300ML;;;1;;;Non;;4;;;;Lancé;00004;52;550" \
                       u";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;" \
                        ";;;;;;;;;;;Non;;6821077" \
                       u";CCBDC11;5960396;;;Non;;;;;0;0;0;0;101;;GIVAUDAN (pour les parfums);" \
                        "86,57;;;;," \
                       u"75;;;;;;;;;;101;0;0;448;;;;;;;; "

        parsed_line = self.dofn.process(line_article, self.schema, self.file_path, self.separator).next()

        expected_values = ["1210v0", "1210", "0", "Archivé", "", "3660005183077", "0",
                           "Y00381A001A", "18307", "18307", "", "", "", "", "15/01/2013",
                           "01/06/2019", "01/01/2018", "", "", "SVC NUTRI 2 MASQ NUTRI P150ML",
                           "NUTRI SILKY MASK DRY HAIR 150ML",
                           "Nutrition - Masque Nutri Soyeux Pot 150Ml",
                           "Nutrition - Nutri Silky Mask 150 Ml Jar", "", "0", "1.24", "", "36",
                           "N1302", "S1608", "7", "", "2", "1", "",
                           "Une efficacité boostée grâce à l'avocat relipidant et l'action des "
                           "fructanes d'ageve dès le cuir chevelu, une texture sensorielle, un "
                           "nouveau parfum addictif et ce dans un tube ultra pratique.",
                           "Une efficacité boostée grâce à l'avocat relipidant et l'action des"
                           " fructanes d'ageve dès le cuir chevelu, une texture sensorielle, "
                           "un nouveau parfum addictif et ce dans un tube ultra pratique.",
                           "Produit inclus dans le relancement juin 2019",
                           "Produit inclus dans le relancement juin 2019", "", "Oui", "MARK",
                           "15/01/2013", "01/08/2016", "Non", "", "EUR", "Archivé", "Archived",
                           "1", "Y00381A001", "1", "Oui", "", "", "", "122169", "", "", "",
                           "https://assetsp.keepeek.com/pm_74_122_122169-067b412189-preview.jpg",
                           "https://assetsp.keepeek.com/pm_74_122_122169-067b412189-preview.jpg",
                           "SOIN VEGETAL CAP 2 MASQUE CHEVEUX NUTRI-SOYEUX  POT 150ML",
                           "SOIN VEGETAL CAP 2 NUTRI-SILKY HAIR MASK  POT 150ML", "", "", "", "",
                           "1", "", "", "CA|CZ|FR|GB|HU|MA|PL|RO|RU|SK|UA|US", "01|03", "", "",
                           "5292v0", "", "0011", "195.05", "150", "G", "", "", "", "18/04/2018",
                           "22/03/2019", "", "553v0", "553", "0", "Archivé", "Non", "Y00381A", "",
                           "", "", "278", "3305900000", "1", "", "Oui", "", "", "71", "15/01/2013",
                           "01/08/2016", "15/01/2013", "01/06/2019", "SVC Masque Chev",
                           "SVC Nutri-Silky", "SVC-Masque Cheveux Nutri-Soyeux-150ML",
                           "SVC-Masque Cheveux Nutri-Soyeux-150ML",
                           "Soin Vegetal Cap 2 Nutrition Masque Cheveux Nutri-Soyeux  150ML",
                           "Soin Vegetal Cap 2 Nutrition Nutri-Silky Hair Mask  150ML",
                           "Nutrition - Masque Soin Nutri-Soyeux",
                           "Nutrition - Masque Soin Nutri-Soyeux", "", "", "2", "Non", "MARK", "",
                           "1", "36", "93", "93", "", "", "42", "", "42", "", "0", "93", "93", "",
                           "", "Non", "", "", "Non", "", "", "", "9", "IE", "IE", "0006", "Non", "",
                           "", "1", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                           "", "MM", "", "", "", "", "", "", "", "", "Masque Cheveux Nutri 150ML",
                           "Nutri-Silky Hair Mas 150ML", "", "", "GO", "", "", "", "", "", "", "",
                           "", "", "", "", "1", "", "1", "", "", "", "", "", "", "", "", "", "",
                           "", "553v1", "", "", "", "18/04/2018", "06/10/2018", "", "0006",
                           "381v0", "381", "0", "Archivé", "", "", "41", "", "", "Y00381", "",
                           "15/01/2013", "01/06/2019", "Masque Cheveux Nutri-Soyeux",
                           "Nutri-Silky Hair Mask", "Masque Chev", "Nutri-Silky",
                           "Masque Cheveux Nutri", "Nutri-Silky Hair Mas", "", "", "", "Non", "",
                           "06", "", "", "01", "", "", "Non", "0", "", "", "", "", "", "", "", "",
                           "", "00004", "00033", "00000330", "521", "", "", "", "87", "", "",
                           "101", "", "", "", "", "", "", "", "1", "15/01/2013", "01/08/2016",
                           "Oui", "", "", "", "", "", "", "", "", "", "", "17", "", "4", "", "",
                           "06", "", "", "", "165", "18/04/2018", "27/09/2018", "278",
                           "15/01/2013", "01", "", "", "", "", "", "", "",
                           "SVC 15/01/2013 Shampooing Texturisant",
                           "launch of SVC BRILLANCE SH 300ML", "", "", "1", "", "", "Non", "", "4",
                           "", "", "", "Lancé", "00004", "52", "550", "", "", "", "", "", "", "",
                           "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                           "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                           "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                           "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
                           "Non", "", "6821077", "CCBDC11", "5960396", "", "", "Non", "", "", "",
                           "", "0", "0", "0", "0", "101", "", "GIVAUDAN (pour les parfums)",
                           "86,57", "", "", "", ",75", "", "", "", "", "", "", "", "", "", "101",
                           "0", "0", "448", "", "", "", "", "", "", "", "",
                           "DonneesMetierMDMProduits.csv", "2019-04-19 12:08:43.306188 UTC"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time']:
                self.assertEqual(parsed_line[column], expected_values[idx])
