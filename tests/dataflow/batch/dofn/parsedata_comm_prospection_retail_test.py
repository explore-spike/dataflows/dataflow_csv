# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class ParsedataDoFnTest(TestCase):
    """
    sample data avec numero de version
    84410|"7583"|31703|"4176"|5032909180|"605020658"|0|1|2|"[2],[99],[39]"|0|0|"19000101"|0|"19000101"|0|"19000101"|0.00|0.00|0|0.00|0|0.00|0|0.00|0|0.00|0.00|0.00000|0.00|0.00|0.00|"20150526 11:26:20"|0.399|0.399|0.000|0.00|0.000|0.000|0.00|0.00|6521|"20141113"|6521|"20141113"|0|0|0|1|0|0|0

    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "PRS_ALZ_GCP_YRFR_D_DET.TXT"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/prospection_retail.json')
        self.version = 5

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_prospection_vad(self):
        line_prospection = u'84410|"7583"|31703|"4176"|5032909180|"605020658"|0|1|2|"[2],[99],[39]"|0|0|"19000101"|0|"19000101"|0|"19000101"|0.00|0.00|0|0.00|0|0.00|0|0.00|0|0.00|0.00|0.00000|0.00|0.00|0.00|"20150526 11:26:20"|0.399|0.399|0.000|0.00|0.000|0.000|0.00|0.00|6521|"20141113"|6521|"20141113"|0|0|0|1|0|0|0'

        parsed_line = self.dofn.process(line_prospection, self.schema, self.file_path,
                                        self.separator, self.version).next()

        expected_values = ["84410","7583","31703","4176","5032909180","605020658","0","1","2","[2],[99],[39]","0","0","19000101","0","19000101","0","19000101","0.00","0.00","0","0.00","0","0.00","0","0.00","0","0.00","0.00","0.00000","0.00","0.00","0.00","20150526 11:26:20","0.399","0.399","0.000","0.00","0.000","0.000","0.00","0.00","6521","20141113","6521","20141113","0","0","0","1","0","0","0"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time', 'version_number']:
                self.assertEqual(parsed_line[column], expected_values[idx])
            elif column == 'version_number':
                self.assertEqual(parsed_line[column], self.version)
        print expected_values
