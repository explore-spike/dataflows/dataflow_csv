# coding=utf-8
from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class ParsedataDoFnTest(TestCase):
    """
    sample data
    04C87B82B52F8D83E053CD12A8C0BA79|
    949628892|VPM|7fa18124bc2414525f04edbe7eef26c880eeab51cce7eae9c26b42accb406771|
    df340084d26e78450d143675c49c586485e87ce351ebb42664a4c58a89359ca8|
    f0b408497864a4885aa704b3b4f60f3c8f802742c75da451690490a6a7dfeb95|
    d7f9756f640fd6a66204b5290c5d91a286ac40087707cc2d85aa4c8f192e29a3
    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "SWP_ALZ_GCP_YRFR_D_DET.txt"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/legacy_agg_media_retail.json')
        self.version = "201905"

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_history_socle(self):
        line_swb = u'5033294492|"980100267"|1|"[14],[27],[46]"|6799|"20150803"|' \
                        '0|1|1|9.75|0.00|"20170310 17:07:30"' \


        parsed_line = self.dofn.process(line_swb, self.schema, self.file_path,
                                        self.separator, self.version).next()

        expected_values = ["5033294492","980100267","1","[14],[27],[46]","6799","20150803","0","1","1","9.75",
                           "0.00","20170310 17:07:30"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time', 'version_number']:
                self.assertEqual(parsed_line[column], expected_values[idx])
            elif column == 'version_number':
                self.assertEqual(parsed_line[column], self.version)
        print expected_values
