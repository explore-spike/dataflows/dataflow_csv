from unittest import TestCase

from dataflow.batch.dofn.parsedata import ParseDataDoFn
from utils.utils import get_schema_from_json


class ParsedataDoFnTest(TestCase):
    """
    sample data avec numero de version
    2067508382|1331040081|1|"2019-05-27 11:00:59"|693240862|1|962800192|"eb1b6fc60849c36b70545df453c6fddac794f7261ec83fa37ffe8731d712529a"||||1||"401702091"
    """

    def setUp(self):
        self.dofn = ParseDataDoFn()
        self.separator = '|'
        self.file_path = "EXTRACT_TRACKING_ECRM_GCP_YRFR.TXT"
        self.schema = get_schema_from_json('resources/bq/DDL/schema/tracking_ecrm.json')
        self.version = 5

    # write the input data and check the output for each column of the header table
    def test_columns_extraction_prospection_vad(self):
        line_prospection = u'2067508382|1331040081|1|"2019-05-27 ' \
                           u'11:00:59"|693240862|1|962800192' \
                           u'|"eb1b6fc60849c36b70545df453c6fddac794f7261ec83fa37ffe8731d712529a"||||1||"401702091" '

        parsed_line = self.dofn.process(line_prospection, self.schema, self.file_path,
                                        self.separator, self.version).next()

        expected_values = ["2067508382", "1331040081", "1", "2019-05-27 11:00:59", "693240862", "1", "962800192",
                           "eb1b6fc60849c36b70545df453c6fddac794f7261ec83fa37ffe8731d712529a", "", "", "", "1", "",
                           "401702091"]

        for idx, column in enumerate(self.schema):
            print(column)
            if column not in ['source_file_name', 'processing_time']:
                self.assertEqual(parsed_line[column], expected_values[idx])

        print expected_values
