"""
    Module contenant des fonctions utiles
"""
import re
from datetime import datetime
from google.cloud import bigquery
from collections import OrderedDict
import json


def extract_file_name(file_path):
    """
    Extract filename from file path
    :param file_path: File path : str
    :return: Filename : str
    """
    return re.search(r"[\w.]+$", file_path.strip()).group(0)


def convert_to_date(date_string):
    """
    Convert date from format 'YYYYMMdd' ( Ex: 20191202) to 'YYYY-MM-dd' (Ex: 02-12-2019)
    :param date_string: Date au format 'YYYYMMdd' : str
    :return: Date to format 'YYYY-MM-dd' : str
    """
    return str(datetime.strptime(date_string, '%Y%m%d').date())


def parse_bqstr_schema(schema_string):
    """
    Convert BigQuery string schema to dict
    :param schema_string: str
    :return: BQ Schema : dict
    """
    # on recupere le schema BigQuery en format string
    schema = schema_string.split(',')

    # on parse et affecte le nom de la colonne et son type
    column_name = [column.split(':')[0] for column in schema]
    column_type = [column.split(':')[1] for column in schema]

    # on cree le dictionnaire contenant le nom de la colonne et son type
    return dict(zip(column_name, column_type))


def encode_to_str_utf8(var_to_encode):
    """
    Encode en String utf-8
   :param var_to_encode: unicode a encoder : unicode
    :return: String utf-8 : str
    """
    return str(var_to_encode.encode("utf-8"))


# def extract_schema(dataset_id, table_id):
#     """
#     Extrait le schema d'une table dans BigQuery
#     :param dataset_id: Id du Dataset de destination: str
#     :param table_id: Id ou nom de la table : str
#     :return: Schema de la table : SchemaField
#     """
#     client = bigquery.Client()
#
#     dataset_ref = client.dataset(dataset_id)
#     table_ref = dataset_ref.table(table_id)
#     table = client.get_table(table_ref)
#
#     # View table properties
#     return table.schema


def extract_schema(fq_table_id):
    """
    Extrait le schema d'une table dans BigQuery
    :param fq_table_id: Nom complet de la table => PROJECT:DATASET.TABLE : str
    :return: Schema de la table : SchemaField
    """

    project_id = fq_table_id.split(':', 1)[0]
    dataset_id, table_id = tuple(fq_table_id.split(':', 1)[1].split('.'))

    client = bigquery.Client(project=project_id)

    dataset_ref = client.dataset(dataset_id)
    table_ref = dataset_ref.table(table_id)
    table = client.get_table(table_ref)

    # View table properties
    return table.schema


def parse_schema_dict(schema):
    """
    Converti un schema de BigQuery en dict
    :param schema: SchemaField
    :return: Schema en dictionnaire : dict
    """
    column_name = [encode_to_str_utf8(field.name) for field in schema]
    column_type = [encode_to_str_utf8(field.field_type) for field in schema]

    return OrderedDict(zip(column_name, column_type))


def get_schema_from_json(path_to_schema):
    """
    Converti un schema fichier json en dict
    :param path_to_schema: Chemin du fichier json
    :return: Schema en dictionnaire : dict
    """
    schema = json.load(open(path_to_schema))
    column_name = [encode_to_str_utf8(field['name']) for field in schema]
    column_type = [encode_to_str_utf8(field['type']) for field in schema]
    return OrderedDict(zip(column_name, column_type))
