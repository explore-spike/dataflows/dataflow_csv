# Introduction 
C'est un programme utilisant Apache Beam (Python SDK) qui :

Transfert de données brutes d'un fichier texte de la RL1 vers la Raw layer 2 dans Big Query (RL2)
Les données sont stockées en format "string"
La colonne "processing_time" et la colonne "source_file_name" sont ajoutées

Les différents paramètres du Dataflow
skip_header_lines : nombre de ligne à ignorer au début de fichier
id_header_line : identifiant de la ligne d'entete, exemple #
column_version : numéro de la colonne contenant le numero de version, la numérotation commence à 0

exemple fichier prospection_vad
        --skip_header_lines 0
        --id_header_line #
        --column_version 3


### Fichier traité
````
Profile matching
History socle
Prospection_vad
````
# Running on GCP

### Authentication
```
gcloud auth application-default login
```
### Run on GCP

```
PYTHON -m main \
        --input GCS_FILE_PATH
        --table_name TABLE_NAME
        --separator |
        --runner DataflowRunner
        --project PROJECT_ID
        --temp_location GCS_TEMP_DIR
        --staging_location GCS_STAGING_DIR
        --region europe-west1
        --zone europe-west1-b
        --setup_file ./setup.py
        --subnetwork SUBNETWORK_URL
        --service_account_email SERVICE_ACCOUNT_EMAIL
        --skip_header_lines SKIP_HEADER_LINES
        --id_header_line #
        --column_version 1
```

### Run on local

```
PYTHON -m main \
        --input GCS_FILE_PATH
        --separator |
        --table_name TABLE_NAME
        --project PROJECT_ID
        --skip_header_lines SKIP_HEADER_LINES
        --id_header_line #
        --column_version 1
```

#Package and distribute sources to Google Cloud composer
```
python setup.py sdist 
tar -xzvf dist/dataflow_batch_profilematching_rl1_rl2-0.1.0.tar.gz -C dist
gsutil -m cp -r dist/dataflow_batch_profilematching_rl1_rl2-0.1.0 gs://COMPOSER_BUCKET/dags
```

# Conventions

## BigQuery 
Datasets, tables, and column names sould all be lower case and words separeted by underscore, and no leading underscore  