#!/usr/bin/env bash

#set PYTHON to your virtual env interpreter
PYTHON=venv/bin/python

# On dataFlow
PYTHON -m main \
        --input GCS_FILE_PATH
        --table_name TABLE_NAME
        --separator |
        --runner DataflowRunner
        --project PROJECT_ID
        --temp_location GCS_TEMP_DIR
        --staging_location GCS_STAGING_DIR
        --region europe-west1
        --zone europe-west1-b
        --setup_file ./setup.py
        --subnetwork SUBNETWORK_URL
        --service_account_email SERVICE_ACCOUNT_EMAIL
        --skip_header_lines SKIP_HEADER_LINES
        --id_header_line #
        --column_version 1


# On Local
PYTHON -m main \
        --input GCS_FILE_PATH
        --table_name TABLE_NAME
        --separator |
        --project PROJECT_ID
        --skip_header_lines SKIP_HEADER_LINES
        --id_header_line #
        --column_version 1

#Profilematching Dataflow
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-rl-customer-profilematching/20190505/20190505_17830_INFOS_CLIENTES_PROFILMATCHEES_extrait \
        --table_name mydata-raw-yrfr-dev-601adc:rl2.customer_profilematching \
        --project mydata-raw-yrfr-dev-601adc \
        --separator | \
        --runner DataflowRunner \
        --region europe-west1 \
        --zone europe-west1-b \
        --setup_file ./setup.py \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-profilematching \
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
        --skip_header_lines 0

#History_socle Dataflow
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-landing-zone/sales-retail-histo-20190522/VTE_SOC_GCP_YRFR_D_HIS.txt \
        --table_name mydata-raw-yrfr-dev-601adc:rl2_history.history_socle \
        --project mydata-raw-yrfr-dev-601adc \
        --separator | \
        --runner DataflowRunner \
        --region europe-west1 \
        --zone europe-west1-b \
        --setup_file ./setup.py \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-historysocle \
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
        --skip_header_lines 1

#ProspectionVad Dataflow
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-rl-prospection-vad/20190524/PRS_DIA_GCP_YRFR_D_DET.txt \
        --table_name mydata-raw-yrfr-dev-601adc:rl2.prospection_vad \
        --project mydata-raw-yrfr-dev-601adc \
        --separator | \
        --runner DataflowRunner \
        --region europe-west1 \
        --zone europe-west1-b \
        --setup_file ./setup.py \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-prospection-vad \
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
        --skip_header_lines 0
        --id_header_line #
        --column_version 3

#trafic_retail Dataflow
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-rl-traffic/20190611/CPT_LYR_DAT_YRFR_V_CPT.TXT \
        --table_name mydata-raw-yrfr-dev-601adc:rl2.trafic_retail \
        --project mydata-raw-yrfr-dev-601adc \
        --separator "|" \
        --runner DataflowRunner \
        --region europe-west1 \
        --zone europe-west1-b \
        --setup_file ./setup.py \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-trafic-test \
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
        --skip_header_lines 0
        --id_header_line "#"
        --column_version 1


# tracking-ecrm
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-landing-zone/tracking-ecrm/EXTRACT_TRACKING_ECRM_GCP_YRFR.txt \
        --table_name mydata-raw-yrfr-dev-601adc:rl2.tracking_ecrm  \
        --skip_header_lines 1
        --project mydata-raw-yrfr-dev-601adc  \
        --separator "|"  \
        --runner DataflowRunner  \
        --region europe-west1  \
        --zone europe-west1-b  \
        --setup_file ./setup.py  \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-tracking-ecrm
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/  \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/  \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe  \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com  \


#ProspectionRetail Dataflow
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-landing-zone/prospection_retail/20190618/PRS_ALZ_GCP_YRFR_D_DET.txt \
        --table_name mydata-raw-yrfr-dev-601adc:rl2.prospection_retail \
        --project mydata-raw-yrfr-dev-601adc \
        --separator "|" \
        --runner DataflowRunner \
        --region europe-west1 \
        --zone europe-west1-b \
        --setup_file ./setup.py \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-prospection-retail \
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
        --skip_header_lines 0 \
        --id_header_line "#" \
        --column_version 3 

#legacyAggMediaRetail Dataflow
python main.py \
        --input gs://bck-mydata-collectstore-yrfr-dev-rl-legacy-agg-media-retail/20190621/SWP_ALZ_GCP_YRFR_D_DET.txt \
        --table_name mydata-raw-yrfr-dev-601adc:rl2.legacy_agg_media_retail \
        --project mydata-raw-yrfr-dev-601adc \
        --separator "|" \
        --runner DataflowRunner \
        --region europe-west1 \
        --zone europe-west1-b \
        --setup_file ./setup.py \
        --job_name mydata-raw-yrfr-dev-rl1-rl2-legacy-agg-media-retail \
        --temp_location gs://bck-mydata-raw-yrfr-dev-dataflow-temp/ \
        --staging_location gs://bck-mydata-raw-yrfr-dev-dataflow-staging/ \
        --subnetwork https://www.googleapis.com/compute/v1/projects/global-shared-8c5b40/regions/europe-west1/subnetworks/sub-gcp-global-shared-mydata-app-npe \
        --service_account_email sa-intk-mydata-yrfr-dev@mydata-raw-yrfr-dev-601adc.iam.gserviceaccount.com \
        --skip_header_lines 0 \
        --id_header_line "#" \
        --column_version 3


