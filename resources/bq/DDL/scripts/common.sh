#!/usr/bin/env bash

if [[ "$#" -eq 1 ]];
then
    PROJECT_ID="$1"
    echo "INFO: project_id : $PROJECT_ID"
else
    echo "ERROR: project_id is mandatory"
    exit 1
fi

DATASET_ID="rl2"
DATASET_HISTORY_ID="rl2_history"

echo "INFO: dataset_id (default) : $DATASET_ID"

# List of tables
export TABLES_LIST_INFO=(
    "customer_profilematching"
    "history_socle"
    "traffic_retail"
    "prospection_vad"
    "prospection_retail"
    "product_reference"
    "product"
    "tracking_ecrm"
    "legacy_agg_media_retail"
)

