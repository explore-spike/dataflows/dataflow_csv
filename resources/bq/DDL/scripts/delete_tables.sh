#!/usr/bin/env bash

current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${current_dir}/common.sh"

# CREATE TABLES
for table in "${TABLES_LIST_INFO[@]}"; do
    if [[ ${table} == "history_socle" ]] ;then
        echo "### Deleting table ${table} on dataset ${DATASET_HISTORY_ID}..."
        bq rm -f -t "${PROJECT_ID}:${DATASET_HISTORY_ID}.${table}"
    else
        echo "### Deleting table ${table} on dataset ${DATASET_ID}..."
        bq rm -f -t "${PROJECT_ID}:${DATASET_ID}.${table}"
    fi
done