#!/usr/bin/env bash

current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

. "${current_dir}/common.sh"

# CREATE TABLES
for table in "${TABLES_LIST_INFO[@]}"; do
    if [[ ${table} == "history_socle" ]] ;then

        echo "### Creating table ${table} with schema file ${table}.json on dataset ${DATASET_HISTORY_ID}..."
        bq mk \
            --project_id "${PROJECT_ID}" \
            --dataset_id "${DATASET_HISTORY_ID}" \
            --time_partitioning_type=DAY \
            --clustering_fields=source_file_name \
            --table "${table}" \
            "resources/bq/DDL/schema/${table}.json"

        echo "### Table ${table} description"
        bq show \
            --schema \
            --format=prettyjson \
            "${PROJECT_ID}:${DATASET_HISTORY_ID}.${table}"
    else
        echo "### Creating table ${table} with schema file ${table}.json on dataset ${DATASET_ID}..."
        bq mk \
            --project_id "${PROJECT_ID}" \
            --dataset_id "${DATASET_ID}" \
            --time_partitioning_type=DAY \
            --clustering_fields=source_file_name \
            --table "${table}" \
            "resources/bq/DDL/schema/${table}.json"

        echo "### Table ${table} description"
        bq show \
            --schema \
            --format=prettyjson \
            "${PROJECT_ID}:${DATASET_ID}.${table}"
    fi
done

